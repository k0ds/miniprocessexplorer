

#include <iostream>
#include <stdio.h>
#include <windows.h>
#include <Psapi.h>
#include <TlHelp32.h>
#include <wil/resource.h>
#include <string>
#include <WtsApi32.h>
#include <atltime.h>
#include <string>



#pragma comment(lib, "wtsapi32")

#define TH32CS_SNAPPROCESS 0x00000002



void enumPids()
{
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	PWTS_PROCESS_INFO_EX info;
	CONSOLE_SCREEN_BUFFER_INFO csbi = { 0 };
	GetConsoleScreenBufferInfo(hConsole, &csbi);
	COORD coordcur = csbi.dwCursorPosition;
	coordcur.X = 0;
	coordcur.Y = 0;
	


	DWORD count;
	DWORD level = 1;
	if (!::WTSEnumerateProcessesEx(WTS_CURRENT_SERVER_HANDLE, &level, WTS_ANY_SESSION, (PWSTR*)&info, &count))
		return;
	printf("\nPID      IMAGE");

	for (DWORD i = 0; i < count; i++) {
		auto pi = info + i;

		DWORD PID = pi->ProcessId;
		LPWSTR PROCESSNAME = pi->pProcessName;

		printf("\n %5d    %ws",PID, PROCESSNAME);

	}
	SetConsoleCursorPosition(hConsole, coordcur);
	::WTSFreeMemoryExW(WTSTypeProcessInfoLevel1, info, count);

	return;
}


void KillProcess()
{
	system("cls");


	int pid = 0;
	char pidInput[MAX_PATH];
	printf("\n\nEnter pid to kill: ");                           
	enumPids();
	fgets(pidInput, sizeof(pidInput), stdin);
	
	pid = std::stoi(pidInput);
		
	

	 
	HANDLE hProcess = OpenProcess(PROCESS_TERMINATE, FALSE, pid); //open handle to process to be killed
	
	if (TerminateProcess(hProcess, EXIT_SUCCESS) == NULL) //if it fails then throw error
	{
		DWORD error = ::GetLastError();
		printf("\nCould not kill Process id: %d  Error: %d\n", pid,  error);
	}
	else
	{
		printf("\nSuccesfully killed process : %d\n", pid);
	}

	
	return;
}



VOID KeyEventProc(KEY_EVENT_RECORD ker)
{
	

	if (ker.wVirtualKeyCode == 0x4B) //if key == 'k'
		KillProcess();
	return;
}
	

void printComputerName() //sort of useless but why not
{
	char compName[MAX_COMPUTERNAME_LENGTH + 1];
	DWORD size = sizeof(compName);
	GetComputerNameA(compName, &size);
	printf("Computer Name: %s\n\n", compName);

}


CString GetUserNameFromSid(PSID sid) { 
	if (sid == nullptr)
		return L"";
	WCHAR name[128], domain[64];
	DWORD len = _countof(name);
	DWORD domainLen = _countof(domain);
	SID_NAME_USE use;
	if (!::LookupAccountSid(nullptr, sid, name, &len, domain, &domainLen, &use))
		return L"";

	return CString(domain) + L"\\" + name;
}

CString GetCpuTime(PWTS_PROCESS_INFO_EXW pi){
	auto totalTime = pi->KernelTime.QuadPart + pi->UserTime.QuadPart;
	return CTimeSpan(totalTime / 10000000LL).Format(L"%D:%H:%M:%S");
}




bool EnableDebugPrivilege() {
	wil::unique_handle hToken;
	if (!::OpenProcessToken(::GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES, hToken.addressof()))
		return false;

	TOKEN_PRIVILEGES tp;
	tp.PrivilegeCount = 1;
	tp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;
	if (!::LookupPrivilegeValue(nullptr, SE_DEBUG_NAME, &tp.Privileges[0].Luid))
		return false;

	if (!::AdjustTokenPrivileges(hToken.get(), FALSE, &tp, sizeof(tp), nullptr, nullptr))
		return false;

	return ::GetLastError() == ERROR_SUCCESS;
}

bool EnumerateProcesses2(HANDLE hConsole) {
	PWTS_PROCESS_INFO_EX info;

	CONSOLE_SCREEN_BUFFER_INFO csbi = { 0 };
	GetConsoleScreenBufferInfo(hConsole, &csbi);
	COORD coordcur = csbi.dwCursorPosition;


	DWORD count;
	DWORD level = 1;
	if (!::WTSEnumerateProcessesEx(WTS_CURRENT_SERVER_HANDLE, &level, WTS_ANY_SESSION, (PWSTR*)&info, &count))
		return false;
	printf("\nPID    SID   THREADS    HANDLES  CPUTIME         USER              IMAGE");

	for (DWORD i = 0; i < count; i++) {
		auto pi = info + i;

		DWORD PID = pi->ProcessId;
		DWORD SESSIONID = pi->SessionId;
		DWORD THREADS = pi->NumberOfThreads;
		DWORD HANDLES = pi->HandleCount;
		PCWSTR CPUTIME = (PCWSTR)GetCpuTime(pi);
		PCWSTR USERNAME = (PCWSTR)GetUserNameFromSid(pi->pUserSid);
		LPWSTR PROCESSNAME = pi->pProcessName;

		printf("\n %5d  %d    %3d       %4d     %ws    %ws   %ws",
			PID, SESSIONID, THREADS, HANDLES,
			CPUTIME,
			USERNAME, PROCESSNAME);
		
	}                                                                      //i think it might be onto something here, setting the cursor position somewhere here
	coordcur.X = 0;
	coordcur.Y = 0;
	SetConsoleCursorPosition(hConsole, coordcur);
	::WTSFreeMemoryExW(WTSTypeProcessInfoLevel1, info, count);

	return true;
}

int main(void) 
{
	
	DWORD fdwSaveOldMode;

	/**if (!EnableDebugPrivilege()) {
		printf("Failed to enable Debug privilege!\n");
	}*/
	std::wstring strW = L"Mini Process Explorer";
	SetConsoleTitle(strW.c_str());  //set the console title because why not
	
	HANDLE hStdin = GetStdHandle(STD_INPUT_HANDLE);  //code to enable input in console
	INPUT_RECORD irInBuf[128];
	if (!GetConsoleMode(hStdin, &fdwSaveOldMode))
		printf("GetConsoleMode");
	DWORD fdwMode = ENABLE_WINDOW_INPUT | ENABLE_MOUSE_INPUT;
	if (!SetConsoleMode(hStdin, fdwMode))
		printf("SetConsoleMode");
	DWORD eventsNum = 0;

	

	HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE);//get handle to console
						
	printComputerName();

	int pid = 0;
	
	
	while (true)
	{
		 
		
		EnumerateProcesses2(hStdout);

		if (!ReadConsoleInput(hStdin, irInBuf, 128, &eventsNum)) //if no input just keep running
		{
			
			continue;

		}
		else
		{
			for (int i = 0; i < eventsNum; i++)
			{
				switch (irInBuf[i].EventType)
				{
				case KEY_EVENT: // keyboard input
					KeyEventProc(irInBuf[i].Event.KeyEvent);
					system("cls");
					break;
				case MOUSE_EVENT:
					break;

				case FOCUS_EVENT:
					break;
				
				case MENU_EVENT:
					break;

				case WINDOW_BUFFER_SIZE_EVENT:
					break;


				default:
					break;
				}
			}

		}
		Sleep(1000);
				                        													
	
		
		  
		
	
	}
	CloseHandle(hStdout);
	

		
		

	


	SetConsoleMode(hStdin, fdwSaveOldMode);

	return 0;
}
